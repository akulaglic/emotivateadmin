package com.tristan.comp.emotivate.userinterface.widgets

import android.content.Context
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.TextView
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.utilities.TypeFaceProvider

/**
 * Notes:
 *
 *
 * //Write notes here
 *
 * ------------------------------------------------------------------
 *
 * @author: Džemal Ibrić
 * 26/03/2019
 * <dzemal.ibric></dzemal.ibric>@klika.ba>
 */
class EmotivateTextView : TextView {

    private var fontName: String? = null

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context) : super(context) {
        init(null)
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.CustomView)
            fontName = if (fontName != null) fontName else a.getString(R.styleable.CustomView_fontName)
            if (fontName != null) {
                typeface = TypeFaceProvider.getTypeFace(context, fontName!!)
                paintFlags = paintFlags or Paint.SUBPIXEL_TEXT_FLAG
            }
            a.recycle()
        }
    }
}
