package com.tristan.comp.emotivate.userinterface.fragments.production

import com.tristan.comp.emotivate.data.GymRealmCache
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.userinterface.fragments.base.BaseViewPagerFragment

/**
 * Created by Comp on 27.9.2018..
 */
class GymQuotesFragment : BaseViewPagerFragment() {

    override fun getQuotes(): ArrayList<Quote> {
        val list = ArrayList<Quote>()
        val realmList = GymRealmCache().getGymQuotes(Constants.GYM_CATEGORY_ID)!!.gymQuotes
        realmList.forEach { quote : Quote -> list.add(quote) }
        return list
    }
}