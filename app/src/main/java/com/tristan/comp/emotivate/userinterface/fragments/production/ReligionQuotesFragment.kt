package com.tristan.comp.emotivate.userinterface.fragments.production

import com.tristan.comp.emotivate.data.ReligionRealmCache
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.userinterface.fragments.base.BaseViewPagerFragment

/**
 * Created by Azra on 28.9.2018.
 */
class ReligionQuotesFragment : BaseViewPagerFragment() {

    override fun getQuotes(): ArrayList<Quote> {
        val list = ArrayList<Quote>()
        val realmList = ReligionRealmCache().getReligionQuotes(Constants.RELIGION_CATEGORY_ID)!!.religionQuotes
        realmList.forEach { quote : Quote -> list.add(quote) }
        return list
    }
}