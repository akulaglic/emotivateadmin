package com.tristan.comp.emotivate.userinterface.activity.main

import android.content.Intent
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.userinterface.activity.main.landing.LandingActivity
import com.tristan.comp.emotivate.userinterface.base.EmotivateBaseActivity
import com.tristan.comp.emotivate.userinterface.dialogs.AddQuoteDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_drawer.*

/**
 * Created by Comp on 27.7.2018..
 */

class EmotivateMainActivity : EmotivateBaseActivity<MainNavigationController>(),
        NavigationView.OnNavigationItemSelectedListener {

    override fun initializeScreen() {
        val toolbar = findViewById<Toolbar>(R.id.dashboard_toolbar)
        initToolbar(toolbar)
        initDrawerLayout(toolbar)
        val navigationView = findViewById<NavigationView>(R.id.dashboard_nav_view)
        initNavMenu(navigationView)
        floatingAdd.setOnClickListener(onFloatingButtonClick)
        getNavigationController()?.goToProductionFragment()
    }

    override fun getFragmentContainerId(): Int {
        return R.id.fragmentContainer
    }

    override fun createNavigationController(): MainNavigationController {
        return MainNavigationController(this)
    }

    override fun getLayoutRId(): Int {
        return R.layout.activity_main_drawer
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        dashboardDrawerLayout.closeDrawers()
        when (item.itemId) {
            R.id.production -> {
                getNavigationController()?.goToProductionFragment()
                return true
            }
            R.id.development -> {
                getNavigationController()?.goToDevelopmentFragment()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    val onFloatingButtonClick = View.OnClickListener {
        AddQuoteDialog(this).show()
    }

    private fun initToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
    }

    private fun initNavMenu(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener(this)
    }

    private fun initDrawerLayout(toolbar: Toolbar) {
        val drawer = findViewById<DrawerLayout>(R.id.dashboardDrawerLayout)
        val toggle = ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)

        drawer.addDrawerListener(toggle)
        toggle.syncState()
    }

}
