package com.tristan.comp.emotivate.services
import android.app.Notification
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.data.shared_preferences.EmotivateSharedPreferences
import com.tristan.comp.emotivate.userinterface.activity.main.EmotivateMainActivity
import com.tristan.comp.emotivate.utilities.Constants

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 22/10/2018
 * <dzemal.ibric@klika.ba>
 */
class NotificationPusherService(val mContext : Context) {

    val NOTIFICATION_ID = "NotificationId"

    fun createNotification(notificationText : String) {
        val mBuilder = NotificationCompat.Builder(mContext, NOTIFICATION_ID)
                .setSmallIcon(R.mipmap.ic_logo_black)
                .setContentTitle("Emotivate")
                .setContentText(notificationText)
                .setLargeIcon(BitmapFactory.decodeResource(
                        mContext.resources,
                        R.mipmap.ic_logo_green))
                .setColor(Color.WHITE)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(notificationText))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentIntent(createPendingIntent())
        if (EmotivateSharedPreferences(mContext).getBoolean(Constants.NOTIFICATION_SOUND))
            mBuilder.setDefaults(Notification.DEFAULT_SOUND)

       // notificationManager.notify(0, mBuilder.build());
        with(NotificationManagerCompat.from(mContext)) {
            // notificationId is a unique int for each notification that you must define
            notify(1, mBuilder.build())
        }
    }
    fun createPendingIntent () : PendingIntent? {
        val resultIntent = Intent(mContext, EmotivateMainActivity::class.java)
        val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(mContext).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        return  resultPendingIntent
    }
}