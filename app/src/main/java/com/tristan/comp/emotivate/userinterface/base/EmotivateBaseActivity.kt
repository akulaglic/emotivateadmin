package com.tristan.comp.emotivate.userinterface.base

import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.tristan.comp.emotivate.R

/**
 * Created by Comp on 24.7.2018..
 */
abstract class EmotivateBaseActivity<NAVIGATION_CONTROLLER : BaseNavigationController> : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(getLayoutRId())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private var navigationController: NAVIGATION_CONTROLLER? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.navigationController = createNavigationController()
        val layoutRId = getLayoutRId()
        if (layoutRId != 0) {
            setContentView(layoutRId)
        }
        if (savedInstanceState == null) {
            setStatusBarColor()
            initializeScreen()
        }
        onActivityCreated()
    }

    /**
     * Called at the end of onCreate
     */
    protected fun onActivityCreated() {
        // Do nothing
    }

    /**
     * @return layout resource id of this activity
     */
    protected abstract fun getLayoutRId(): Int


    /**
     * Called when activity has just started and has no state stored
     * Use this to start first fragment
     */
    protected abstract fun initializeScreen()

    /**
     * You should always use fragments, and use activity just to provide
     * context and navigation controller to the fragments
     * Override this method to provide fragment container id, so [NavigationController]
     * can switch out fragments easily
     * @return ViewGroup id of the fragment container
     */
    abstract fun getFragmentContainerId(): Int

    /**
     * Called when activity is created
     * This method should always return a new navigation controller
     * This will be called whenever activity is recreated
     * @return instance of navigation controller
     */
    protected abstract fun createNavigationController(): NAVIGATION_CONTROLLER

    /**
     * Provides current activities navigation controller
     */
    fun getNavigationController(): NAVIGATION_CONTROLLER? {
        return navigationController
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(R.color.status_bar_color)
        }
    }

}