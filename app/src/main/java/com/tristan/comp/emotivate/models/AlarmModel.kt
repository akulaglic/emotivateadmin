package com.tristan.comp.emotivate.models

import io.realm.RealmObject

/**
 * Created by Azra on 13.5.2019.
 */
open class AlarmModel(var time: String = "Set time",
                      var turnedOn: Boolean = false) : RealmObject(){
}