package com.tristan.comp.emotivate.userinterface.activity.main

import com.tristan.comp.emotivate.userinterface.activity.main.fragment.ProductionFragment
import com.tristan.comp.emotivate.userinterface.activity.main.fragment.DevelopmentFragment
import com.tristan.comp.emotivate.userinterface.base.BaseNavigationController


/**
 * Created by Comp on 27.7.2018..
 */
class MainNavigationController(emotivateMainActivity: EmotivateMainActivity) :
        BaseNavigationController(emotivateMainActivity) {

    fun goToProductionFragment() {
        changeFragment(ProductionFragment())
    }

    fun goToDevelopmentFragment() {
        changeFragment(DevelopmentFragment())
    }

}