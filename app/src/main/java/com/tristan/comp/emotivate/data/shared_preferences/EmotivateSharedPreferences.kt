package com.tristan.comp.emotivate.data.shared_preferences

import android.content.Context

/**
 * Created by Azra on 16.5.2019.
 */
class EmotivateSharedPreferences(val mContext : Context) {

    private val TAG = "emotivate_shared_preferences"

    fun putBoolean(key :String, value : Boolean) {
        mContext.getSharedPreferences(TAG, Context.MODE_PRIVATE).edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key :String) : Boolean {
        return mContext.getSharedPreferences(TAG, Context.MODE_PRIVATE).getBoolean(key, false)
    }
}