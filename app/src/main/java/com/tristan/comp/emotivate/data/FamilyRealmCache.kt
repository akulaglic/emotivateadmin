package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.FamilyQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.FriendshipQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm

/**
 * Created by Azra on 28.9.2018.
 */
open class FamilyRealmCache : BaseRealmCache<FamilyQuoteModel>(FamilyQuoteModel::class.java) {

    fun saveFamilyQuotes(familyQuoteModel: FamilyQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, familyQuoteModel) }
    }

    fun getFamilyQuotes(id : Int) : FamilyQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }

    fun hasFamilyQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }

    fun setModelChecked(checked: Boolean) {
        val familyQuoteModel = Realm.getDefaultInstance().where(FamilyQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        familyQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }
}