package com.tristan.comp.emotivate.userinterface.adapters

import android.app.TimePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CompoundButton
import android.widget.Switch
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.data.AlarmRealmCache
import com.tristan.comp.emotivate.models.AlarmCacheModel
import com.tristan.comp.emotivate.models.AlarmModel
import com.tristan.comp.emotivate.userinterface.adapters.base.BaseListAdapter
import com.tristan.comp.emotivate.userinterface.widgets.EmotivateTextView
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.utilities.ListUtilities
import com.tristan.comp.emotivate.utilities.StringUtilities
import com.tristan.comp.emotivate.utilities.managers.EmotivateNotificationManager
import io.realm.RealmList
import java.util.*

/**
 * Created by Azra on 13.5.2019.
 */
class AlarmListAdapter(mContext: Context?,
                       alarmListModels: ArrayList<AlarmModel>) : BaseListAdapter<AlarmModel>(mContext, alarmListModels) {

    val mNotificationManager = EmotivateNotificationManager(mContext)

    override val layoutResourceId: Int
        get() = R.layout.item_alarm

    override fun setupView(position: Int, view: View?): View? {
        val switch = view?.findViewById<Switch>(R.id.alarmSwitch)
        val text = view?.findViewById<EmotivateTextView>(R.id.alarmTime)
        //Disable switch if time is not yet set
        switch?.isEnabled = mModels[position].time != "Set time"
        view?.setOnClickListener(getItemClickListener(position, switch, text))
        switch?.setOnCheckedChangeListener(getSwitchCheckedChangedListener(position))
        switch?.isChecked = mModels[position].turnedOn
        text?.text = mModels[position].time
        return view!!
    }

    fun getItemClickListener(position: Int,
                             switch: Switch?,
                             text: EmotivateTextView?) = View.OnClickListener {

        val hour = if (!switch?.isEnabled!!) Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        else StringUtilities.extractHourFromString(mModels[position].time)
        val minute = if (!switch.isEnabled) Calendar.getInstance().get(Calendar.MINUTE)
        else StringUtilities.extractMinuteFromString(mModels[position].time)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
                mContext,
                TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                    run {
                        switch.isEnabled = true
                        setAlarm(selectedHour, selectedMinute, position)
                        text?.text = StringUtilities.convertTimeToString(selectedHour, selectedMinute)
                        mModels[position].time = StringUtilities.convertTimeToString(selectedHour, selectedMinute)
                        switch.isChecked = true

                    }
                },
                hour,
                minute,
                true)
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    fun getSwitchCheckedChangedListener(position: Int) = CompoundButton.OnCheckedChangeListener {
        compoundButton: CompoundButton, isChecked: Boolean ->

        mModels[position].turnedOn = isChecked
        if (!isChecked) {
            cancelAlarm(position)
        } else {
            setAlarm(StringUtilities.extractHourFromString(mModels[position].time),
                    StringUtilities.extractMinuteFromString(mModels[position].time),
                    position)
        }
        AlarmRealmCache().saveAlarms(AlarmCacheModel(Constants.ALARM_ID,
                ListUtilities.convertArrayListToRealmList(mModels) as RealmList<AlarmModel>))
    }


    fun setAlarm(hour: Int,
                 minute: Int,
                 alarmId: Int) {
        mNotificationManager.setNotificationAlarm(hour, minute, alarmId)
    }

    fun cancelAlarm(alarmId: Int) {
        mNotificationManager.cancelAlarm(alarmId)
    }
}