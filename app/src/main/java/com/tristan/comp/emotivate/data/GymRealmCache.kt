package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.GymQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric@klika.ba>
 */
open class GymRealmCache : BaseRealmCache<GymQuoteModel>(GymQuoteModel::class.java) {

    fun saveGymQuotes(gymQuoteModel: GymQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, gymQuoteModel) }
    }

    fun getGymQuotes(id : Int) : GymQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }
    fun hasGymQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }
    fun setModelChecked(checked: Boolean) {
        val gymQuoteModel = Realm.getDefaultInstance().where(GymQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        gymQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }
}