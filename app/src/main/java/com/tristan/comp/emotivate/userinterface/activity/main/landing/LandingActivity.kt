package com.tristan.comp.emotivate.userinterface.activity.main.landing

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.userinterface.activity.main.EmotivateMainActivity
import kotlinx.android.synthetic.main.menu_options_dialog.*
import android.content.Intent
import android.support.v7.app.AlertDialog
import com.tristan.comp.emotivate.userinterface.adapters.MenuOptionsAdapter
import android.widget.ListView
import android.widget.Toast
import com.tristan.comp.emotivate.data.*
import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.utilities.Constants

/**
 * Created by Azra on 9.5.2019.
 */
class LandingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_options_dialog)

        val adapter = MenuOptionsAdapter(this, MenuRealmCache().getProductionModelsFromCache())

        val listView = findViewById<ListView>(R.id.optionsList)
        listView.setAdapter(adapter)

        btnConfirm.setOnClickListener {
            for (menuListModel: MenuListModel in adapter.menuListModels) {
                when (menuListModel.modelText) {
                    Constants.FAMILY_NAME -> FamilyRealmCache().setModelChecked(menuListModel.modelChecked)
                    Constants.FRIENDSHIP_NAME -> FriendshipRealmCache().setModelChecked(menuListModel.modelChecked)
                    Constants.GYM_NAME -> GymRealmCache().setModelChecked(menuListModel.modelChecked)
                    Constants.LIFE_NAME -> LifeRealmCache().setModelChecked(menuListModel.modelChecked)
                    Constants.LOVE_NAME -> LoveRealmCache().setModelChecked(menuListModel.modelChecked)
                    Constants.RELIGION_NAME -> ReligionRealmCache().setModelChecked(menuListModel.modelChecked)
                    Constants.WORK_NAME -> WorkRealmCache().setModelChecked(menuListModel.modelChecked)

                }
            }
            if (IsAllUnchecked(adapter.menuListModels as ArrayList<MenuListModel>)) {

                val builder = AlertDialog.Builder(this)
                builder.setTitle("Warning")
                builder.setMessage("Please select at least one category.")

                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    Toast.makeText(applicationContext,
                            android.R.string.yes, Toast.LENGTH_SHORT).show()
                }

                builder.show()
            }
            else {
                val intent = Intent(this, EmotivateMainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    fun IsAllUnchecked(adapterList: ArrayList<MenuListModel>): Boolean {
        for (menuListModel: MenuListModel in adapterList) {
            when (menuListModel.modelChecked) {
                true -> return false
            }
        }
        return true
    }
}


