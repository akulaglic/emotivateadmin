package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.LifeQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.LoveQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm

/**
 * Created by Comp on 27.9.2018..
 */
open class LoveRealmCache : BaseRealmCache<LoveQuoteModel>(LoveQuoteModel::class.java){

    fun saveLoveQuotes(loveQuoteModel: LoveQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, loveQuoteModel) }
    }

    fun getLoveQuotes(id : Int) : LoveQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }

    fun hasLoveQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }

    fun setModelChecked(checked: Boolean) {
        val loveQuoteModel = Realm.getDefaultInstance().where(LoveQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        loveQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }
}