package com.tristan.comp.emotivate.models

import io.realm.RealmModel
import io.realm.RealmObject

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 27/03/2019
 * <dzemal.ibric@klika.ba>
 */
open class Quote(var author : String = "",
                 var quote : String = "") : RealmObject()