package com.tristan.comp.emotivate.services

import android.app.IntentService
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.tristan.comp.emotivate.userinterface.activity.main.EmotivateMainActivity
import com.tristan.comp.emotivate.utilities.generators.NotificationTextGenerator

/**
 * Created by Comp on 14.5.2019..
 */
class NotificationService : IntentService("") {

    override fun onHandleIntent(intent: Intent?) {
        NotificationPusherService(baseContext).createNotification(getNotificationText())
    }

    private fun getNotificationText() : String {
        return NotificationTextGenerator.generateNotification()
    }

}