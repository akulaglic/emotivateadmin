package com.tristan.comp.emotivate.userinterface.activity.main.fragment

import android.support.design.widget.TabLayout
import android.view.View
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.data.MenuRealmCache
import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.userinterface.activity.main.MainNavigationController
import com.tristan.comp.emotivate.userinterface.adapters.ProductionMenuViewPagerAdapter
import com.tristan.comp.emotivate.userinterface.base.EmotivateBaseFragment
import kotlinx.android.synthetic.main.menu_fragment.view.*

/**
 * Created by Azra on 6.8.2018.
 */
class ProductionFragment : EmotivateBaseFragment<MainNavigationController>() {

    override fun initView(view: View) {
        val checkedModels = getCheckedModels(MenuRealmCache().getProductionModelsFromCache())
        val menuViewPagerAdapter = ProductionMenuViewPagerAdapter(activity,
                checkedModels,
                childFragmentManager)
        view.mfViewPager.adapter = menuViewPagerAdapter
        view.mfTabLayout.setupWithViewPager(view.mfViewPager)
        view.mfViewPager.currentItem = 0
        view.mfTabLayout.tabMode = if (checkedModels.size > 4) TabLayout.MODE_SCROLLABLE
                                     else TabLayout.MODE_FIXED
    }


    override fun getLayoutRId(): Int {
        return R.layout.menu_fragment
    }

    fun getCheckedModels (menuModels: ArrayList<MenuListModel>) : ArrayList<MenuListModel>{
        val checkedMenuModels = ArrayList<MenuListModel>()
        for(menuModel : MenuListModel in menuModels) {
            if(menuModel.modelChecked) checkedMenuModels.add(menuModel)
        }

        return checkedMenuModels
    }
}