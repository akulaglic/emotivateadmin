package com.tristan.comp.emotivate.utilities.generators
import com.tristan.comp.emotivate.data.MenuRealmCache
import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.userinterface.fragments.production.*
import com.tristan.comp.emotivate.utilities.StringUtilities
import java.util.*


/**
 * Created by Azra on 15.5.2019.
 */
class NotificationTextGenerator {
    companion object {
        fun generateNotification() : String {
            val menuListModels = MenuRealmCache().getProductionModelsFromCache()
            val quotesList = ArrayList<String>()
            quotesList += generateListFromChoosenModel(menuListModels)
            val random = Random()

            var notificationText = ""
            for (i in 0 until quotesList.size) {
                val randomIndex = random.nextInt(quotesList.size)
                notificationText = quotesList[randomIndex]
                quotesList.removeAt(randomIndex)
            }

            return notificationText
        }

        fun generateListFromChoosenModel (menuListModels : ArrayList<MenuListModel>) : ArrayList<String>{
            val quoteList = ArrayList<String>()
            for (menuList : MenuListModel in menuListModels) {
                if(menuList.modelChecked) {
                   if(menuList.modelText.equals("Family")) {
                       var quote = FamilyQuotesFragment().getQuotes()
                       for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                   }
                    if(menuList.modelText.equals("Friendship")) {
                        var quote = FriendshipDevQuotesFragment().getQuotes()
                        for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                    }
                    if(menuList.modelText.equals("GYM")) {
                        var quote = GymDevQuotesFragment().getQuotes()
                        for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                    }
                    if(menuList.modelText.equals("Life")) {
                        var quote = LifeDevQuotesFragment().getQuotes()
                        for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                    }
                    if(menuList.modelText.equals("Love")) {
                        var quote = LoveQuotesFragment().getQuotes()
                        for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                    }
                    if(menuList.modelText.equals("Religion")) {
                        var quote = ReligionDevQuotesFragment().getQuotes()
                        for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                    }
                    if(menuList.modelText.equals("Work")) {
                        var quote = WorkDevQuotesFragment().getQuotes()
                        for (i : Quote in quote) quoteList += StringUtilities.ReturnStringInQuotes(i.quote)
                    }
                }
            }
            return quoteList
        }
    }
}