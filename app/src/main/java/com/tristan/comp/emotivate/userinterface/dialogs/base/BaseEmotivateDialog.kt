package com.tristan.comp.emotivate.userinterface.dialogs.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 02/04/2019
 * <dzemal.ibric@klika.ba>
 */
abstract class BaseEmotivateDialog(mContext : Context) : Dialog(mContext) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRId())
        setupDialog()
    }

    abstract fun getLayoutRId() : Int

    open fun setupDialog() {}

}