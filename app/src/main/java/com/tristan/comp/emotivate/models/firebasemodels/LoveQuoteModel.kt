package com.tristan.comp.emotivate.models.firebasemodels

import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Comp on 27.9.2018..
 */
open class LoveQuoteModel (@PrimaryKey var id: Int = 0,
                           var loveQuotes: RealmList<Quote> = RealmList(),
                           var checked: Boolean = true,
                           var name: String = Constants.LOVE_NAME) : RealmObject() {
}