package com.tristan.comp.emotivate.data.firebase

import com.google.firebase.database.FirebaseDatabase

/**
 * Created by Azra on 19.5.2019.
 */
class FirebaseManager {

    fun addQuote(category : String, environment: String, quote : String, author : String) {
        FirebaseDatabase.getInstance().reference.child(
                if (environment == "Production") "categories" else "categories_dev")
                .child(category)
                .push().setValue(Quote(quote, author))
    }
    class Quote(val quote : String = "", val author : String = "")
}