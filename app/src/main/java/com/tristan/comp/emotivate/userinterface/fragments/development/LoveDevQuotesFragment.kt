package com.tristan.comp.emotivate.userinterface.fragments.production

import com.tristan.comp.emotivate.data.LoveRealmCache
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.userinterface.fragments.base.BaseViewPagerFragment

/**
 * Created by Comp on 27.9.2018..
 */
class LoveDevQuotesFragment() : BaseViewPagerFragment() {

    override fun getQuotes(): ArrayList<Quote> {
        val list = ArrayList<Quote>()
        val realmList = LoveRealmCache().getLoveQuotes(Constants.LOVE_CATEGORY_DEV_ID)!!.loveQuotes
        realmList.forEach { quote : Quote -> list.add(quote) }
        return list
    }
}