package com.tristan.comp.emotivate.services

import com.google.firebase.database.*
import com.tristan.comp.emotivate.data.*
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.models.firebasemodels.*
import com.tristan.comp.emotivate.utilities.ListUtilities
import io.realm.Realm
import io.realm.RealmList

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric@klika.ba>
 */
class FirebaseQuoteFetcherService(val fetchingListener: FirebaseQuoteFetchingListener) {

    private val database = FirebaseDatabase.getInstance().reference

    fun getQuotesFromFirebase() {
        // Attach a listener to read the data at our posts reference
        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val hashMap = dataSnapshot.value
                        as HashMap<String, HashMap<String, HashMap<String, HashMap<String, String>>>>
                for ((key, value) in hashMap) {
                    when (key) {
                        Constants.CATEGORIES -> {
                            for ((categoriesKey, categoriesValue) in value) {
                                when (categoriesKey) {
                                    Constants.FAMILY ->
                                        FamilyRealmCache().saveFamilyQuotes(
                                                FamilyQuoteModel(Constants.FAMILY_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.FRIENDSHIP ->
                                        FriendshipRealmCache().saveFriendshipQuotes(
                                                FriendshipQuoteModel(Constants.FRIENDSHIP_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.LIFE ->
                                        LifeRealmCache().saveLifeQuotes(
                                                LifeQuoteModel(Constants.LIFE_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.GYM ->
                                        GymRealmCache().saveGymQuotes(
                                                GymQuoteModel(Constants.GYM_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.LOVE ->
                                        LoveRealmCache().saveLoveQuotes(
                                                LoveQuoteModel(Constants.LOVE_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.WORK ->
                                        WorkRealmCache().saveWorkQuotes(
                                                WorkQuoteModel(Constants.WORK_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.RELIGION ->
                                        ReligionRealmCache().saveReligionQuotes(
                                                ReligionQuoteModel(Constants.RELIGION_CATEGORY_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))

                                }

                            }
                        }
                        Constants.CATEGORIES_DEV -> {
                            for ((categoriesKey, categoriesValue) in value) {
                                when (categoriesKey) {
                                    Constants.FAMILY ->
                                        FamilyRealmCache().saveFamilyQuotes(
                                                FamilyQuoteModel(Constants.FAMILY_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.FRIENDSHIP ->
                                        FriendshipRealmCache().saveFriendshipQuotes(
                                                FriendshipQuoteModel(Constants.FRIENDSHIP_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.LIFE ->
                                        LifeRealmCache().saveLifeQuotes(
                                                LifeQuoteModel(Constants.LIFE_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.GYM ->
                                        GymRealmCache().saveGymQuotes(
                                                GymQuoteModel(Constants.GYM_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.LOVE ->
                                        LoveRealmCache().saveLoveQuotes(
                                                LoveQuoteModel(Constants.LOVE_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.WORK ->
                                        WorkRealmCache().saveWorkQuotes(
                                                WorkQuoteModel(Constants.WORK_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                    Constants.RELIGION ->
                                        ReligionRealmCache().saveReligionQuotes(
                                                ReligionQuoteModel(Constants.RELIGION_CATEGORY_DEV_ID,
                                                        ListUtilities.convertJSONArrayToRealmList(categoriesValue),
                                                        true))
                                }
                            }
                        }
                    }
                }
                fetchingListener.onComplete()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("The read failed: " + databaseError.code)
                fetchingListener.onCanceled()
            }
        })

    }

    interface FirebaseQuoteFetchingListener {
        fun onComplete()
        fun onCanceled()
    }
}