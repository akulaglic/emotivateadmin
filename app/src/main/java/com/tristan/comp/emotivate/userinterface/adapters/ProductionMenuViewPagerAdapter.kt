package com.tristan.comp.emotivate.userinterface.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.userinterface.fragments.production.*
import com.tristan.comp.emotivate.utilities.Constants

/**
 * Created by Comp on 27.9.2018..
 */
class ProductionMenuViewPagerAdapter(val context: Context?,
                                     val menuListModels: ArrayList<MenuListModel>,
                                     fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return generateFragment(menuListModels[position].modelText)
    }

    override fun getCount(): Int {
        return menuListModels.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return menuListModels[position].modelText
    }

    fun generateFragment(fragmentName : String) : Fragment{
       return when (fragmentName) {
            Constants.FAMILY_NAME -> FamilyQuotesFragment()
            Constants.FRIENDSHIP_NAME -> FriendshipDevQuotesFragment()
            Constants.GYM_NAME -> GymDevQuotesFragment()
            Constants.LIFE_NAME -> LifeDevQuotesFragment()
            Constants.LOVE_NAME -> LoveQuotesFragment()
            Constants.RELIGION_NAME -> ReligionDevQuotesFragment()
            Constants.WORK_NAME -> WorkDevQuotesFragment()
            else-> FamilyQuotesFragment()
        }

    }
}