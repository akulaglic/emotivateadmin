package com.tristan.comp.emotivate.userinterface.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.userinterface.viewholders.QuoteItemViewHolder

/**
 * Created by Comp on 27.9.2018..
 */
class QuoteItemsAdapter(val list: ArrayList<Quote>) : RecyclerView.Adapter<QuoteItemViewHolder>() {

    override fun onBindViewHolder(holder: QuoteItemViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuoteItemViewHolder {
        return QuoteItemViewHolder.getViewHolder(parent)
    }
}