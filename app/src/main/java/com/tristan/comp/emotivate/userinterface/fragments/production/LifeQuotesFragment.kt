package com.tristan.comp.emotivate.userinterface.fragments.production

import com.tristan.comp.emotivate.data.LifeRealmCache
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.userinterface.fragments.base.BaseViewPagerFragment

/**
 * Created by Azra on 28.9.2018.
 */
class LifeQuotesFragment : BaseViewPagerFragment() {

    override fun getQuotes(): ArrayList<Quote> {
        val list = ArrayList<Quote>()
        val realmList = LifeRealmCache().getLifeQuotes(Constants.LIFE_CATEGORY_ID)!!.lifeQuotes
        realmList.forEach { quote: Quote -> list.add(quote) }
        return list
    }
}