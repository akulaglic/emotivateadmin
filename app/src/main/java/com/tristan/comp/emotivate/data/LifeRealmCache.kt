package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.LifeQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm

/**
 * Created by Azra on 22.9.2018.
 */
open class LifeRealmCache  : BaseRealmCache<LifeQuoteModel>(LifeQuoteModel::class.java) {

    fun saveLifeQuotes(lifeQuoteModel: LifeQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, lifeQuoteModel) }
    }

    fun getLifeQuotes(id : Int) : LifeQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }

    fun hasLifeQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }

    fun setModelChecked(checked: Boolean) {
        val lifeQuoteModel = Realm.getDefaultInstance().where(LifeQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        lifeQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }
}