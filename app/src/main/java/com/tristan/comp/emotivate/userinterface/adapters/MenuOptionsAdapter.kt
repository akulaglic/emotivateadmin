package com.tristan.comp.emotivate.userinterface.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.CompoundButton
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.userinterface.adapters.base.BaseListAdapter
import com.tristan.comp.emotivate.userinterface.widgets.EmotivateTextView

/**
 * Created by Azra on 7.8.2018.
 */
class MenuOptionsAdapter(mContext: Context?,
                         var menuListModels: ArrayList<MenuListModel>) : BaseListAdapter<MenuListModel>(mContext, menuListModels) {

    override fun setupView(position: Int, view: View?): View? {
        val checkBox = view?.findViewById<CheckBox>(R.id.optionsCheck)
        val text = view?.findViewById<EmotivateTextView>(R.id.optionsText)
        val menuListModel = menuListModels[position]
        checkBox?.setOnCheckedChangeListener { compoundButton: CompoundButton, isChecked: Boolean ->
            menuListModels[position].modelChecked = isChecked
        }
        checkBox?.isChecked = menuListModel.modelChecked
        text?.text = menuListModel.modelText

        return view!!
    }

    override val layoutResourceId: Int
        get() = R.layout.menu_row




}