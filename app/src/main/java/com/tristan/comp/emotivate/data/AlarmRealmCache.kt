package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.AlarmCacheModel
import com.tristan.comp.emotivate.models.firebasemodels.FamilyQuoteModel
import io.realm.Realm

/**
 * Created by Azra on 13.5.2019.
 */
open class AlarmRealmCache : BaseRealmCache<AlarmCacheModel>(AlarmCacheModel::class.java) {

    fun saveAlarms(alarmModel: AlarmCacheModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, alarmModel) }
    }

    fun getAlarms() : AlarmCacheModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).findFirst()) }
    }

    fun hasAlarms(): Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).findFirst()) != null }
    }
}