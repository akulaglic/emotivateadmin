package com.tristan.comp.emotivate.userinterface.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.ArrayAdapter
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.data.firebase.FirebaseManager
import com.tristan.comp.emotivate.userinterface.dialogs.base.BaseEmotivateDialog
import kotlinx.android.synthetic.main.menu_options_dialog.*
import kotlinx.android.synthetic.main.new_quote_popup.*

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 02/04/2019
 * <dzemal.ibric@klika.ba>
 */
class AddQuoteDialog(mContext : Context) : BaseEmotivateDialog(mContext) {

    override fun getLayoutRId(): Int {
        return R.layout.new_quote_popup
    }

    override fun setupDialog() {
        btnAdd.setOnClickListener(onConfirm)
        spinnerEnvironment.adapter = ArrayAdapter<String>(context, R.layout.spinner_item, createEnvironmentList())
        spinnerCategory.adapter = ArrayAdapter<String>(context, R.layout.spinner_item, createCategoryList())
    }

    val onConfirm = View.OnClickListener {
        if (etQuote.text.toString().isEmpty() or
                etAuthor.text.toString().isEmpty() or
                (spinnerCategory.selectedItem == null) or
                (spinnerEnvironment.selectedItem == null)) {
            AlertDialog.Builder(context)
                    .setTitle("Something went wrong")
                    .setMessage("Populate all fields")
                    .show()
        } else {
            FirebaseManager().addQuote(
                    spinnerCategory.selectedItem as String,
                    spinnerEnvironment.selectedItem as String,
                    etQuote.text.toString(),
                    etAuthor.text.toString())
            dismiss()
        }
    }
    fun createEnvironmentList() : ArrayList<String> {
        return arrayListOf("Production", "Development")
    }

    fun createCategoryList() : ArrayList<String> {
        return arrayListOf("family", "friendship", "gym", "life", "love", "religion", "work")
    }

}