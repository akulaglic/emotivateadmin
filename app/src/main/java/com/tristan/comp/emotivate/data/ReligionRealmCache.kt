package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.LoveQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.ReligionQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm

/**
 * Created by Azra on 28.9.2018.
 */
open class ReligionRealmCache : BaseRealmCache<ReligionQuoteModel>(ReligionQuoteModel::class.java){

    fun saveReligionQuotes(religionQuoteModel: ReligionQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, religionQuoteModel) }
    }

    fun getReligionQuotes(id : Int) : ReligionQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }

    fun hasReligionQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }

    fun setModelChecked(checked: Boolean) {
        val religinQuoteModel = Realm.getDefaultInstance().where(ReligionQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        religinQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }
}