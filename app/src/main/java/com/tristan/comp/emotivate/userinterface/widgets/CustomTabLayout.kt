package com.tristan.comp.emotivate.userinterface.widgets

import android.content.Context
import java.lang.reflect.AccessibleObject.setAccessible
import android.support.design.widget.TabLayout
import android.util.AttributeSet


/**
 * Created by Azra on 18.5.2019.
 */
class CustomTabLayout : TabLayout {
    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        try {
            if (tabCount == 0)
                return
            val field = TabLayout::class.java.getDeclaredField("mScrollableTabMinWidth")
            field.isAccessible = true
            field.set(this, (measuredWidth / tabCount.toFloat()).toInt())
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}