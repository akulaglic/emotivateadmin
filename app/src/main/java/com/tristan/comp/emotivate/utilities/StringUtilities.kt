package com.tristan.comp.emotivate.utilities

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 26/03/2019
 * <dzemal.ibric@klika.ba>
 */
class StringUtilities {
    companion object {
        fun ReturnStringInQuotes(string: String) : String {
            return '"' + string + '"'
        }

        fun convertTimeToString(hour:Int, min:Int) : String {
            val hourString = if (hour in (0..9)) "0" + hour.toString() else hour.toString()
            val minString = if (min in (0..9)) "0" + min.toString() else min.toString()
            return hourString + ":" + minString
        }

        fun extractHourFromString(time : String) : Int {
            return time.split(":").toTypedArray()[0].toInt()
        }

        fun extractMinuteFromString(time : String) : Int {
            return time.split(":").toTypedArray()[1].toInt()
        }
    }
}