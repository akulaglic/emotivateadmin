package com.tristan.comp.emotivate.models.firebasemodels

import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Azra on 28.9.2018.
 */
open class WorkQuoteModel (@PrimaryKey var id: Int = 0,
                           var workQuotes: RealmList<Quote> = RealmList(),
                           var checked: Boolean = true,
                           var name: String = Constants.WORK_NAME) : RealmObject() {
}