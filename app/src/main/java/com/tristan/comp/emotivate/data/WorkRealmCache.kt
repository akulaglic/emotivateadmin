package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.ReligionQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm



/**
 * Created by Azra on 28.9.2018.
 */
open class WorkRealmCache : BaseRealmCache<WorkQuoteModel>(WorkQuoteModel::class.java){

    fun saveWorkQuotes(workQuoteModel: WorkQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, workQuoteModel) }
    }

    fun getWorkQuotes(id : Int) : WorkQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }

    fun hasWorkQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }

    fun setModelChecked(checked: Boolean) {
        val workQuoteModel = Realm.getDefaultInstance().where(WorkQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        workQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }

}