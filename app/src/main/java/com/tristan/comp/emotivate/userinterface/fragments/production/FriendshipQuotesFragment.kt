package com.tristan.comp.emotivate.userinterface.fragments.production

import com.tristan.comp.emotivate.data.FriendshipRealmCache
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import com.tristan.comp.emotivate.userinterface.fragments.base.BaseViewPagerFragment

/**
 * Created by Azra on 28.9.2018.
 */
class FriendshipQuotesFragment : BaseViewPagerFragment() {

    override fun getQuotes(): ArrayList<Quote> {
        val list = ArrayList<Quote>()
        val realmList = FriendshipRealmCache().getFriendshipQuotes(Constants.FRIENDSHIP_CATEGORY_ID)!!.friendshipQuotes
        realmList.forEach { quote : Quote -> list.add(quote) }
        return list
    }
}