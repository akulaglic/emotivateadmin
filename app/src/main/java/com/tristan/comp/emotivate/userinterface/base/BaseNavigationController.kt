package com.tristan.comp.emotivate.userinterface.base

import android.support.v4.app.Fragment

/**
 * Created by Comp on 25.7.2018..
 */
open class BaseNavigationController(var baseActivity: EmotivateBaseActivity<*>) {

    fun changeFragment(fragment : Fragment) {
        baseActivity.supportFragmentManager.beginTransaction().
                replace(baseActivity.getFragmentContainerId(), fragment)
                .commitAllowingStateLoss()
    }

    fun changeFragmentWithBackStack(fragment: Fragment) {
        baseActivity.supportFragmentManager.beginTransaction()
                .replace(baseActivity.getFragmentContainerId(), fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }
}