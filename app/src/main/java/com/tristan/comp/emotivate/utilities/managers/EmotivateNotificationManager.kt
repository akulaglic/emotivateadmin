package com.tristan.comp.emotivate.utilities.managers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.view.Menu
import com.tristan.comp.emotivate.services.NotificationAlarmReceiver
import java.util.*

/**
 * Created by Comp on 14.5.2019..
 */
class EmotivateNotificationManager(val context: Context?) {

    val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val alarmIntent = Intent(context, NotificationAlarmReceiver::class.java)

    fun setNotificationAlarm(hour : Int,
                             minute : Int,
                             alarmId: Int) {

        val alarmStartTime = Calendar.getInstance()
        val now = Calendar.getInstance()
        alarmStartTime.set(Calendar.HOUR_OF_DAY, hour)
        alarmStartTime.set(Calendar.MINUTE, minute)
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1)
        }

        alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                alarmStartTime.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                getPendingIntent(alarmId))
    }

    fun cancelAlarm(alarmId : Int) {
        alarmManager.cancel(getPendingIntent(alarmId))
    }

    fun getPendingIntent(alarmId: Int) : PendingIntent {
        return PendingIntent.getBroadcast(
                context, alarmId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }
}