package com.tristan.comp.emotivate.userinterface.activity.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.SyncStateContract
import android.support.v7.app.AppCompatActivity
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.data.LoveRealmCache
import com.tristan.comp.emotivate.services.FirebaseQuoteFetcherService
import com.tristan.comp.emotivate.userinterface.activity.main.landing.LandingActivity
import com.tristan.comp.emotivate.utilities.Constants
import io.realm.Realm

/**
 * Created by Azra on 7.8.2018.
 */
class SplashActivity : AppCompatActivity(), FirebaseQuoteFetcherService.FirebaseQuoteFetchingListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)
        setStatusBarColor()
        val firebaseQuoteFetcherService = FirebaseQuoteFetcherService(this)
        firebaseQuoteFetcherService.getQuotesFromFirebase()
    }

    override fun onComplete() {
        startMainActivity()
    }

    override fun onCanceled() {
        //To be implemented
    }


    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(R.color.status_bar_color)
        }
    }

    private fun startMainActivity() {
        val intent = Intent(this, EmotivateMainActivity::class.java)
        startActivity(intent)
        finish()
    }
}