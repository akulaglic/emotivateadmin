package com.tristan.comp.emotivate.data.base

import io.realm.RealmObject



/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric@klika.ba>
 */
open class RealmString(var string : String = "") : RealmObject() {
}