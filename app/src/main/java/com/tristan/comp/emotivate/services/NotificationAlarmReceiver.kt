package com.tristan.comp.emotivate.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/**
 * Created by Comp on 14.5.2019..
 */
class NotificationAlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val service1 = Intent(context, NotificationService::class.java)
        context?.startService(service1)
    }
}