package com.tristan.comp.emotivate.userinterface.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Comp on 25.7.2018..
 */
abstract class EmotivateBaseFragment<NAV_CONTROLLER_TYPE : BaseNavigationController> : Fragment() {

    private var navigationController : NAV_CONTROLLER_TYPE? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity is EmotivateBaseActivity<*>) {
            val navigationController = (activity as EmotivateBaseActivity<*>).getNavigationController()
            this.navigationController = navigationController as NAV_CONTROLLER_TYPE
        } else {
            throw RuntimeException("Activity has to extend BaseActivity to use navigation controllers")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(getLayoutRId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
    }

    override fun onDetach() {
        super.onDetach()
        this.navigationController = null
    }

    protected abstract fun getLayoutRId() : Int

    protected fun getNavigationController(): NAV_CONTROLLER_TYPE? {
        return navigationController
    }

    protected abstract fun initView(view: View)


}