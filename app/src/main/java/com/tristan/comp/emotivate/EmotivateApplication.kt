package com.tristan.comp.emotivate

import android.app.Application
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by Comp on 24.7.2018..
 */
class EmotivateApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build()

        Realm.setDefaultConfiguration(realmConfiguration)

        // Open realm to force migration
        Realm.getDefaultInstance().close()
        AppCenter.start(this, "601ccb0c-1321-4d8d-8b6b-aa8066c803c0",
                Analytics::class.java, Crashes::class.java)
        AppCenter.start(this, "601ccb0c-1321-4d8d-8b6b-aa8066c803c0", Analytics::class.java, Crashes::class.java)
    }
}