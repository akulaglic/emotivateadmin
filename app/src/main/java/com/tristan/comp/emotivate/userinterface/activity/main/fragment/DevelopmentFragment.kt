package com.tristan.comp.emotivate.userinterface.activity.main.fragment

import android.view.View
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.userinterface.activity.main.MainNavigationController
import com.tristan.comp.emotivate.userinterface.base.EmotivateBaseFragment
import android.support.design.widget.TabLayout
import com.tristan.comp.emotivate.data.MenuRealmCache
import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.userinterface.adapters.DevelopmentMenuViewPagerAdapter
import com.tristan.comp.emotivate.userinterface.adapters.ProductionMenuViewPagerAdapter
import kotlinx.android.synthetic.main.menu_fragment.view.*
import kotlin.collections.ArrayList


/**
 * Created by Azra on 12.5.2019.
 */
class DevelopmentFragment : EmotivateBaseFragment<MainNavigationController>() {

    override fun initView(view: View) {
        val checkedModels = getCheckedModels(MenuRealmCache().getDevelopmentModelsFromCache())
        val menuViewPagerAdapter = DevelopmentMenuViewPagerAdapter(activity,
                checkedModels,
                childFragmentManager)
        view.mfViewPager.adapter = menuViewPagerAdapter
        view.mfTabLayout.setupWithViewPager(view.mfViewPager)
        view.mfViewPager.currentItem = 0
        view.mfTabLayout.tabMode = if (checkedModels.size > 4) TabLayout.MODE_SCROLLABLE
        else TabLayout.MODE_FIXED
    }


    override fun getLayoutRId(): Int {
        return R.layout.menu_fragment
    }

    fun getCheckedModels (menuModels: ArrayList<MenuListModel>) : ArrayList<MenuListModel>{
        val checkedMenuModels = ArrayList<MenuListModel>()
        for(menuModel : MenuListModel in menuModels) {
            if(menuModel.modelChecked) checkedMenuModels.add(menuModel)
        }

        return checkedMenuModels
    }
}