package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.data.base.BaseRealmCache
import com.tristan.comp.emotivate.models.firebasemodels.FriendshipQuoteModel
import com.tristan.comp.emotivate.models.firebasemodels.WorkQuoteModel
import io.realm.Realm

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric@klika.ba>
 */
open class FriendshipRealmCache : BaseRealmCache<FriendshipQuoteModel>(FriendshipQuoteModel::class.java) {

    fun saveFriendshipQuotes(friendshipQuoteModel: FriendshipQuoteModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, friendshipQuoteModel) }
    }

    fun getFriendshipQuotes(id : Int) : FriendshipQuoteModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) }
    }
    fun hasFriendshipQuotes(id : Int) : Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).equalTo("id", id).findFirst()) != null }
    }
    fun setModelChecked(checked: Boolean) {
        val friendshipQuoteModel = Realm.getDefaultInstance().where(FriendshipQuoteModel::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        friendshipQuoteModel?.checked = checked
        Realm.getDefaultInstance().commitTransaction()
    }
}