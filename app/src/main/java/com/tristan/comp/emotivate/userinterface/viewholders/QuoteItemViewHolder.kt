package com.tristan.comp.emotivate.userinterface.viewholders

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.StringUtilities
import kotlinx.android.synthetic.main.quote_item.view.*

/**
 * Created by Comp on 27.9.2018..
 */
class QuoteItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun getViewHolder(viewGroup: ViewGroup) : QuoteItemViewHolder {
            return QuoteItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.quote_item, viewGroup, false
            ))
        }
    }

    @SuppressLint("SetTextI18n")
    fun bind(quote: Quote) {
        itemView.quoteText.text = StringUtilities.ReturnStringInQuotes(quote.quote)
        itemView.authorText.text = quote.author
    }
}