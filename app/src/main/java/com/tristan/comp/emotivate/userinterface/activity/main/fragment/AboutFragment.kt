package com.tristan.comp.emotivate.userinterface.activity.main.fragment

import android.view.View
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.userinterface.activity.main.MainNavigationController
import com.tristan.comp.emotivate.userinterface.base.EmotivateBaseFragment
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import android.widget.Switch
import android.widget.TextView
import kotlinx.android.synthetic.main.about_fragment.*


/**
 * Created by Azra on 12.5.2019.
 */
class AboutFragment : EmotivateBaseFragment<MainNavigationController>() {

    override fun initView(view: View) {
        mailLink.setOnClickListener {
            startEmailActivity()
        }
        appVersion.text = getAppVersion()
    }

    override fun getLayoutRId(): Int {
        return R.layout.about_fragment
    }


    fun startEmailActivity() {
        val intent = Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("app.emotivate@gmail.com"))
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        startActivity(Intent.createChooser(intent, "Send Email using:"))
    }

    fun getAppVersion(): String? {
        val pInfo = context?.packageManager?.getPackageInfo(context?.packageName, 0)
        return "Version " + pInfo?.versionName
    }

}