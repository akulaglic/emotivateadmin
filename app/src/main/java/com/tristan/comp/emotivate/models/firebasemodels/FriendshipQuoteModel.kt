package com.tristan.comp.emotivate.models.firebasemodels

import com.tristan.comp.emotivate.data.base.RealmString
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric@klika.ba>
 */
open class FriendshipQuoteModel(@PrimaryKey var id: Int = 0,
                                var friendshipQuotes: RealmList<Quote> = RealmList(),
                                var checked: Boolean = true,
                                var name: String = Constants.FRIENDSHIP_NAME) : RealmObject() {

}