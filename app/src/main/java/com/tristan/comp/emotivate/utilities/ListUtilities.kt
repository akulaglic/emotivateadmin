package com.tristan.comp.emotivate.utilities

import com.tristan.comp.emotivate.models.Quote
import io.realm.RealmList

/**
 * Created by Azra on 15.5.2019.
 */
class ListUtilities {
    companion object {

        private val AUTHOR = "author"
        private val QUOTE = "quote"

        fun convertJSONArrayToRealmList(hashMap: HashMap<String, HashMap<String, String>>) : RealmList<Quote> {
            val list : RealmList<Quote> = RealmList()
            for ((key, value) in hashMap) {
                var author = ""
                var quote = ""
                if (value.containsKey(AUTHOR)) {
                    author = value[AUTHOR]!!
                }
                if (value.containsKey(QUOTE)) {
                    quote = value[QUOTE]!!
                }
                list.add(Quote(author, quote))
            }
            return list
        }

        fun convertArrayListToRealmList(array: ArrayList<*>) : RealmList<*>{
            val realmList = RealmList<Any>()
            for (index : Int in (0 until array.size)) run {
                realmList.add(array[index])
            }
            return realmList
        }
    }
}