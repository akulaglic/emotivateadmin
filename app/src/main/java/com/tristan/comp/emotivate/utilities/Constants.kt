package com.tristan.comp.emotivate.utilities

/**
 * Created by Azra on 11.8.2018.
 */
class Constants {

    companion object {
        //String constants
        val GYM = "gym"
        val CATEGORIES = "categories"
        val CATEGORIES_DEV = "categories_dev"
        val FRIENDSHIP = "friendship"
        val LIFE = "life"
        val LOVE = "love"
        val FAMILY = "family"
        val WORK = "work"
        val RELIGION = "religion"

        val GYM_NAME = "GYM"
        val FRIENDSHIP_NAME = "Friendship"
        val LIFE_NAME = "Life"
        val LOVE_NAME = "Love"
        val FAMILY_NAME = "Family"
        val WORK_NAME = "Work"
        val RELIGION_NAME = "Religion"

        //Integer constants
        val GYM_CATEGORY_ID = 1
        val FRIENDSHIP_CATEGORY_ID = 2
        val LIFE_CATEGORY_ID = 3
        val LOVE_CATEGORY_ID = 4
        val FAMILY_CATEGORY_ID = 5
        val WORK_CATEGORY_ID = 6
        val RELIGION_CATEGORY_ID = 7
        val ALARM_ID = 8
        val NOTIFICATION_SOUND = "notification_sound"

        val GYM_CATEGORY_DEV_ID = 9
        val FRIENDSHIP_CATEGORY_DEV_ID = 10
        val LIFE_CATEGORY_DEV_ID = 11
        val LOVE_CATEGORY_DEV_ID = 12
        val FAMILY_CATEGORY_DEV_ID = 13
        val WORK_CATEGORY_DEV_ID = 14
        val RELIGION_CATEGORY_DEV_ID = 15
    }
}