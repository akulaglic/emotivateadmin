package com.tristan.comp.emotivate.userinterface.fragments.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tristan.comp.emotivate.R
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.userinterface.adapters.QuoteItemsAdapter
import kotlinx.android.synthetic.main.viewpager_fragment.view.*

/**
 * Created by Comp on 27.9.2018..
 */
abstract class BaseViewPagerFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.viewpager_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.vfRecyclerView.layoutManager = LinearLayoutManager(context)
        view.vfRecyclerView.adapter = QuoteItemsAdapter(getQuotes())

    }

    abstract fun getQuotes() : ArrayList<Quote>

}