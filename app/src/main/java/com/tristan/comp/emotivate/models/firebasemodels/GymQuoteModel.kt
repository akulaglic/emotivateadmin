package com.tristan.comp.emotivate.models.firebasemodels

import com.tristan.comp.emotivate.data.base.RealmString
import com.tristan.comp.emotivate.models.Quote
import com.tristan.comp.emotivate.utilities.Constants
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric@klika.ba>
 */
open class GymQuoteModel(@PrimaryKey var id: Int = 0,
                         var gymQuotes: RealmList<Quote> = RealmList(),
                         var checked: Boolean = true,
                         var name: String = Constants.GYM_NAME) : RealmObject() {

}