package com.tristan.comp.emotivate.data

import com.tristan.comp.emotivate.models.MenuListModel
import com.tristan.comp.emotivate.utilities.Constants

/**
 * Created by Azra on 12.5.2019.
 */
class MenuRealmCache {

    fun getProductionModelsFromCache() : ArrayList<MenuListModel> {
        val menuListModels = ArrayList<MenuListModel>()
        menuListModels.add(generateMenuListModel(FamilyRealmCache().getFamilyQuotes(Constants.FAMILY_CATEGORY_ID)!!.name,
                FamilyRealmCache().getFamilyQuotes(Constants.FAMILY_CATEGORY_ID)!!.checked))

        menuListModels.add(generateMenuListModel(FriendshipRealmCache().getFriendshipQuotes(Constants.FRIENDSHIP_CATEGORY_ID)!!.name,
                FriendshipRealmCache().getFriendshipQuotes(Constants.FRIENDSHIP_CATEGORY_ID)!!.checked))

        menuListModels.add(generateMenuListModel(GymRealmCache().getGymQuotes(Constants.GYM_CATEGORY_ID)!!.name,
                GymRealmCache().getGymQuotes(Constants.GYM_CATEGORY_ID)!!.checked))

        menuListModels.add(generateMenuListModel(LifeRealmCache().getLifeQuotes(Constants.LIFE_CATEGORY_ID)!!.name,
                LifeRealmCache().getLifeQuotes(Constants.LIFE_CATEGORY_ID)!!.checked))

        menuListModels.add(generateMenuListModel(LoveRealmCache().getLoveQuotes(Constants.LOVE_CATEGORY_ID)!!.name,
                LoveRealmCache().getLoveQuotes(Constants.LOVE_CATEGORY_ID)!!.checked))

        menuListModels.add(generateMenuListModel(ReligionRealmCache().getReligionQuotes(Constants.RELIGION_CATEGORY_ID)!!.name,
                ReligionRealmCache().getReligionQuotes(Constants.RELIGION_CATEGORY_ID)!!.checked))

            menuListModels.add(generateMenuListModel(WorkRealmCache().getWorkQuotes(Constants.WORK_CATEGORY_ID)!!.name,
                WorkRealmCache().getWorkQuotes(Constants.WORK_CATEGORY_ID)!!.checked))
        return menuListModels
    }

    fun getDevelopmentModelsFromCache() : ArrayList<MenuListModel> {
        val menuListModels = ArrayList<MenuListModel>()
        menuListModels.add(generateMenuListModel(FamilyRealmCache().getFamilyQuotes(Constants.FAMILY_CATEGORY_DEV_ID)!!.name,
                FamilyRealmCache().getFamilyQuotes(Constants.FAMILY_CATEGORY_DEV_ID)!!.checked))

        menuListModels.add(generateMenuListModel(FriendshipRealmCache().getFriendshipQuotes(Constants.FRIENDSHIP_CATEGORY_DEV_ID)!!.name,
                FriendshipRealmCache().getFriendshipQuotes(Constants.FRIENDSHIP_CATEGORY_DEV_ID)!!.checked))

        menuListModels.add(generateMenuListModel(GymRealmCache().getGymQuotes(Constants.GYM_CATEGORY_DEV_ID)!!.name,
                GymRealmCache().getGymQuotes(Constants.GYM_CATEGORY_DEV_ID)!!.checked))

        menuListModels.add(generateMenuListModel(LifeRealmCache().getLifeQuotes(Constants.LIFE_CATEGORY_DEV_ID)!!.name,
                LifeRealmCache().getLifeQuotes(Constants.LIFE_CATEGORY_DEV_ID)!!.checked))

        menuListModels.add(generateMenuListModel(LoveRealmCache().getLoveQuotes(Constants.LOVE_CATEGORY_DEV_ID)!!.name,
                LoveRealmCache().getLoveQuotes(Constants.LOVE_CATEGORY_DEV_ID)!!.checked))

        menuListModels.add(generateMenuListModel(ReligionRealmCache().getReligionQuotes(Constants.RELIGION_CATEGORY_DEV_ID)!!.name,
                ReligionRealmCache().getReligionQuotes(Constants.RELIGION_CATEGORY_DEV_ID)!!.checked))

        menuListModels.add(generateMenuListModel(WorkRealmCache().getWorkQuotes(Constants.WORK_CATEGORY_DEV_ID)!!.name,
                WorkRealmCache().getWorkQuotes(Constants.WORK_CATEGORY_DEV_ID)!!.checked))
        return menuListModels
    }

    fun generateMenuListModel(name : String, checked : Boolean) : MenuListModel {
        return MenuListModel(name, checked)
    }
}